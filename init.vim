
call plug#begin()
" Plugins are installed at: '~/.local/share/nvim/plugged/'

Plug 'https://github.com/tpope/vim-commentary' " Vim Commentary.

call plug#end()

" Line numbering
set number

set autoindent " Indentation
set tabstop=4
set shiftwidth=4
set smarttab
set softtabstop=4
set mouse=a " enable mouse click
set clipboard=unnamedplus " using system clipboard

" Autocompletion in command mode.
set wildmenu
set wildmode=longest:full,full
set wildoptions=pum
